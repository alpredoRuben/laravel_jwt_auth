<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Helpers\UserValidator;

/** Exception Handler */
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

use JWTAuth;
use App\User;

class AuthController extends Controller
{
    public function signIn(Request $req) {
        $credentials = $req->only('email','password');
        $token = JWTAuth::attempt($credentials);

        try{
            if(! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'success' > false,
                    'message' => 'invalid credential'
                ], 400);
            }
        }
        catch(JWTException $e){
            return response()->json([
                'success' => false,
                'message' => 'could_not_create_token',
                'error_state' => $e
            ] ,500);
        }

        return response()->json([
            'success' => true,
            'token' => $token
        ], 200);
    }

    public function signUp(Request $req) {
        $valid = new UserValidator($req);

        if($valid == true){
            $user = User::create([
                'username' => $req->get('username'),
                'email' => $req->get('email'),
                'password' => Hash::make($req->get('password')),
            ]);

            $token = JWTAuth::fromUser($user);

            return response()->json([
                'status' => true,
                'message' => 'create new user successfully',
                'data' => compact('user','token')
            ],201);
        }
        else{
            return response()->json([
                'success' => false,
                'message' => $valid
            ], 400);
        }
    }

    public function getAuthUser() {
        $user = JWTAuth::parseToken()->authenticate();

        // try{
        //     if(!$user)
        //         return response()->json([
        //             'success' => false,
        //             'message' => 'user_not_found'
        //         ], 404);
        // }
        // catch(TokenExpiredException $e) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'token_expired'
        //     ], $e->getStatusCode());
        // }
        // catch(TokenInvalidException $e){
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'token_invalid'
        //     ], $e->getStatusCode());
        // }
        // catch(JWTException $e){
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'token_absent'
        //     ], $e->getStatusCode());
        // }

        return response()->json([compact('user')], 200);
    }


}
