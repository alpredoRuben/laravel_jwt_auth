<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class JwtMiddleware extends BaseMiddleware
{
    public function handle($request, Closure $next)
    {
        try{
            $user = JWTAuth::parseToken()->authenticate();
        }
        catch(Exception $e){
            if($e instanceof TokenInvalidException) {
                return response()->json(['status' => 'Token is invalid']);
            }
            else if ($e instanceof TokenExpiredException) {
                return response()->json(['status' => 'Token is Expired']);
            }
            else {
                return response()->json(['status' => 'Authorization Token Not Found']);
            }
        }

        return $next($request);
    }

}
