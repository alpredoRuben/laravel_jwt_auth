<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class UserValidator {

    public function validateRegister(Request $req) {
        $validator = Validator::make($req->all(), [
            'username' => 'required',
            'email' => 'required|unique',
            'password' => 'required|max:50'
        ]);

        if($validator->fails())
            return $validator->errors();
        else
            return true;

    }

}
