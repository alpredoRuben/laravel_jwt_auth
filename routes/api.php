<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// /** @var BEARS */
// Route::post('add/new/bear', 'NatureController@storeBear');
// Route::post('update/bear', 'NatureController@updateBear');
// Route::get('find/bear/by/{id}', 'NatureController@findBearBy');
// Route::get('find/all/bears', 'NatureController@findAllBears');

// Route::get('find/bearfish/{id}', 'BearController@findBearFishBy');


// /** @var FISHES */
// Route::post('add/new/fish', 'NatureController@storeFish');
// Route::post('update/fish', 'NatureController@updateFish');
// Route::get('find/fish/by/{id}', 'NatureController@findFishBy');
// Route::get('find/all/fish', 'NatureController@findAllFish');

// /** @var TREES */
// Route::post('add/new/tree', 'NatureController@storeTree');


// /** @var PICNICS */
// Route::post('add/new/picnic', 'NatureController@storePicnic');

Route::post('register', 'AuthController@signUp');
Route::post('login', 'AuthController@signIn');
Route::get('open', 'DataController@open');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user', 'AuthController@getAuthUser');
    Route::get('closed', 'DataController@closed');
});
